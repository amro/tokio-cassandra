
use std::collections::HashMap;

use crate::cql::Query;
use crate::cql::ResultSet;
use crate::cql::Value;

pub trait QueryResult {
  fn get_string(&self, row: usize, name: &str) -> Option<String>;
  fn get_bool(&self, row: usize, name: &str) -> Option<bool>;
}

impl QueryResult for ResultSet {
  fn get_string(&self, row: usize, name: &str) -> Option<String> {
    self.metadata.as_ref().and_then(|md| {
      for (i, c) in md.iter().enumerate() {
        if c.name == name {
          return self.rows[row].columns[i].as_ref().map(|data| String::from_utf8(data.clone()).unwrap());
        }
      }
      unimplemented!()
    })
  }

  fn get_bool(&self, row: usize, name: &str) -> Option<bool> {
    self.metadata.as_ref().and_then(|md| {
      for (i, c) in md.iter().enumerate() {
        if c.name == name {
          return self.rows[row].columns[i].as_ref().map(|data| data[0] != 0x00);
        }
      }
      unimplemented!()
    })
  }
}

pub struct QueryBuilder {
  query: String,
  positional_parameters: Option<Vec<Value>>,
  named_parameters: Option<HashMap<String, Value>>,
}

impl QueryBuilder {
  pub fn new(query: &str) -> Self {
    QueryBuilder {
      query: query.to_string(),
      positional_parameters: None,
      named_parameters: None,
    }
  }

  fn with_param(mut self, value: Value) -> Self {
    if self.positional_parameters.is_some() {
      self.positional_parameters.as_mut().unwrap().push(value);
      QueryBuilder {
        query: self.query,
        positional_parameters: self.positional_parameters,
        named_parameters: None,
      }
    } else {
      QueryBuilder {
        query: self.query,
        positional_parameters: Some(vec![value]),
        named_parameters: None,
      }
    }
  }

  fn with_named_param(mut self, name: &str, value: Value) -> Self {
    if self.named_parameters.is_some() {
      self.named_parameters.as_mut().unwrap().insert(name.to_string(), value);
      QueryBuilder {
        query: self.query,
        positional_parameters: None,
        named_parameters: self.named_parameters,
      }
    } else {
      let mut params = HashMap::new();
      params.insert(name.to_string(), value);
      QueryBuilder {
        query: self.query,
        positional_parameters: None,
        named_parameters: Some(params),
      }
    }
  }

  pub fn with_text_param(self, value: &str) -> Self {
    self.with_param(Value::Text(value.to_string()))
  }

  pub fn with_named_text_param(self, name: &str, value: &str) -> Self {
    self.with_named_param(name, Value::Text(value.to_string()))
  }

  pub fn with_bool_param(self, value: bool) -> Self {
    self.with_param(Value::Boolean(value))
  }

  pub fn with_named_bool_param(self, name: &str, value: bool) -> Self {
    self.with_named_param(name, Value::Boolean(value))
  }

  pub fn build(self) -> Query {
    Query::new(self.query, self.positional_parameters, self.named_parameters)
  }
}
