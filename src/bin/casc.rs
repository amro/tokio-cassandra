use std::error::Error;

use ::cql::QueryBuilder;
use ::cql::query;
use ::cql::Client;

#[tokio::main]
async fn main() -> Result<(), Box<dyn Error>> {
  env_logger::init();

  let client = Client::connect("127.0.0.1:9042".parse().unwrap()).await?;
  let q = QueryBuilder::new("select * from test.test").build();

  let f1 = client.request(query(q.clone()));
  let f2 = client.request(query(q.clone()));

  println!("f2: {:?}", f2.await?);
  println!("f1: {:?}", f1.await?);
  Ok(())
}

