use futures::stream::SplitSink;
use futures::{SinkExt, StreamExt};

use std::collections::HashMap;
use std::error::Error;
use std::net::SocketAddr;

use tokio::codec::Framed;
use tokio::net::TcpStream;
use tokio::sync::Lock;
use tokio::sync::mpsc::UnboundedSender;
use tokio::sync::mpsc::unbounded_channel;

use crate::codec::CQLCodec;
use crate::codec::RequestId;
use crate::cql;
use crate::cql::Message;

type Sink = SplitSink<Framed<TcpStream, CQLCodec>, (RequestId, Message)>;

struct Shared {
  sink: Sink,
  next_request: RequestId,
}

impl Shared {
  pub fn new(sink: Sink) -> Self {
    Shared {
      sink,
      next_request: RequestId::new(),
    }
  }
}

pub struct Client {
  shared: Lock<Shared>,
  pending: Lock<HashMap<RequestId, UnboundedSender<Message>>>,
}

impl Client {
  fn new(sink: Sink) -> Self {
    Client {
      shared: Lock::new(Shared::new(sink)),
      pending: Lock::new(HashMap::new()),
    }
  }

  pub async fn connect(addr: SocketAddr) -> Result<Self, Box<dyn Error>> {
    let stream = TcpStream::connect(&addr).await?;
    let mut messages = Framed::new(stream, CQLCodec::new());
    messages.send((RequestId::new(), cql::start())).await?;
    if let Some(resp) = messages.next().await {
      match resp {
        Ok((_, Message::Ready)) => {
          let (sink, mut stream) = messages.split();
          let client = Client::new(sink);
          let mut pending = client.pending.clone();

          tokio::spawn(async move {
            loop {
              if let Some(Ok((n, resp))) = stream.next().await {
                let mut m = pending.lock().await;
                if let Some(t) = m.get_mut(&n) {
                  t.send(resp).await.unwrap();
                } else {
                  panic!("unexpected response for stream {:?}", n);
                }
              }
            }
          });
          Ok(client)
        }
        _ => Err("error handling request".into()),
      }
    } else {
      Err("connection closed".into())
    }
  }

  pub async fn request(&self, req: Message) -> Result<Message, Box<dyn Error>> {
    let mut rx = {
      let mut shared = self.shared.clone();
      let mut shared = shared.lock().await;

      let (tx, rx) = unbounded_channel();

      let request_id = shared.next_request;
      shared.next_request = shared.next_request.next();

      {
        let mut pending = self.pending.clone();
        pending.lock().await.insert(request_id, tx);
      }

      shared.sink.send((request_id, req)).await?;
      rx
    };

    if let Some(message) = rx.next().await {
      Ok(message)
    } else {
      Err("request task did not receive a response".into())
    }
  }
}
