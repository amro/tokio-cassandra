use bytes::BufMut;
use bytes::BytesMut;

use log::trace;

use std::io;

use tokio::codec::Decoder;
use tokio::codec::Encoder;

use crate::cql::Message;
use crate::cql::ParseError;
use crate::cql::read_message;
use crate::cql::write_message;

#[derive(Clone, Copy, Debug, Eq, PartialEq, Hash)]
pub struct RequestId(i16);

impl RequestId {
  pub fn new() -> Self {
    RequestId(0)
  }
  pub fn next(&self) -> Self {
    if self.0 < 10000 {
      RequestId(self.0 + 1)
    } else {
      RequestId(0)
    }
  }
}

pub struct CQLCodec;

impl CQLCodec {
  pub fn new() -> Self {
    CQLCodec {

    }
  }
}

impl Decoder for CQLCodec {
  type Item = (RequestId, Message);
  type Error = io::Error;

  fn decode(&mut self, buf: &mut BytesMut) -> Result<Option<(RequestId, Message)>, io::Error> {
    trace!("decoding {}", buf.len());

    trace!("----");
    for b in &buf[..] {
      trace!("{:02X} ", *b);
    }
    trace!("\n----");

    match read_message(&buf) {
      Ok((bytes_read, (request_id, msg))) => {
        trace!("decoded");
        buf.split_to(bytes_read as usize);
        Ok(Some((RequestId(request_id), msg)))
      }
      Err(ParseError::NotEnoughData) => {
        trace!("not enough data {}", buf.len());
        Ok(None)
      }
      Err(ParseError::ParseError) => {
        trace!("invalid");
        Ok(None)
      }
    }
  }
}

impl Encoder for CQLCodec {
  type Item = (RequestId, Message);
  type Error = io::Error;

  fn encode(&mut self, message: (RequestId, Message), buf: &mut BytesMut) -> io::Result<()> {
    let (request_id, msg) = message;
    write_message(request_id.0, &msg, &mut buf.writer()).unwrap();
    Ok(())
  }
}
