use byteorder::BigEndian;
use byteorder::ReadBytesExt;
use byteorder::WriteBytesExt;

use log::info;

use nom::IResult;
use nom::do_parse;
use nom::named;
use nom::number::streaming::be_i16;
use nom::number::streaming::be_i32;
use nom::number::streaming::be_u8;

use std::collections::HashMap;
use std::io::Cursor;
use std::io::Read;
use std::io::Write;
use std::io;
use std::mem;

pub struct Header {
  pub version: u8,
  pub flags: u8,
  pub stream: i16,
  pub opcode: Opcode,
  pub length: i32,
}

#[allow(dead_code)]
#[derive(Debug, Clone, Copy)]
#[repr(i32)]
pub enum ErrorCode {
  ServerError = 0x0000,
  ProtocolError = 0x000A,
  BadCredentials = 0x0100,
  UnavailableException = 0x1000,
  Overloaded = 0x1001,
  IsBootstrapping = 0x1002,
  TruncateError = 0x1003,
  WriteTimeout = 0x1100,
  ReadTimeout = 0x1200,
  SyntaxError = 0x2000,
  Unauthorized = 0x2100,
  Invalid = 0x2200,
  ConfigError = 0x2300,
  AlreadyExists = 0x2400,
  Unprepared = 0x2500,
}

#[derive(Debug)]
pub struct CQLError {
  pub code: ErrorCode,
  pub message: String,
}

#[allow(dead_code)]
#[derive(Debug)]
#[repr(u8)]
pub enum Opcode {
  Error = 0x00,
  Startup = 0x01,
  Ready = 0x02,
  Authenticate = 0x03,
  Options = 0x05,
  Supported = 0x06,
  Query = 0x07,
  Result = 0x08,
  Prepare = 0x09,
  Execute = 0x0A,
  Register = 0x0B,
  Event = 0x0C,
  Batch = 0x0D,
  AuthChallenge = 0x0E,
  AuthResponse = 0x0F,
  AuthSuccess = 0x10,
}

#[allow(dead_code)]
#[derive(Debug, Clone, Copy)]
#[repr(u16)]
pub enum Consistency {
  Any = 0x00,
  One = 0x01,
  Two = 0x02,
  Three = 0x03,
  Quorum = 0x04,
  All = 0x05,
  LocalQuorum = 0x06,
  EachQuorum = 0x07,
  Serial = 0x08,
  LocalSerial = 0x09,
  LocalOne = 0x0A,
}

#[allow(dead_code)]
#[derive(Debug, Clone, Copy)]
#[repr(u16)]
pub enum ColumnTypeId {
  Custom = 0x00,
  Ascii = 0x01,
  Bigint = 0x02,
  Blob = 0x03,
  Boolean = 0x04,
  Counter = 0x05,
  Decimal = 0x06,
  Double = 0x07,
  Float = 0x08,
  Int = 0x09,
  Timestamp = 0x0B,
  Uuid = 0x0C,
  Varchar = 0x0D,
  Varint = 0x0E,
  Timeuuid = 0x0F,
  Inet = 0x10,
  List = 0x20,
  Map = 0x21,
  Set = 0x22,
  UDT = 0x30,
  Tuple = 0x31,
}

#[derive(Debug)]
pub enum Message {
  Error(CQLError),
  Startup(HashMap<String, String>),
  Query(String, Consistency),
  QueryWithParams(String, Vec<Value>, Consistency),
  QueryWithNamedParams(String, HashMap<String, Value>, Consistency),
  Result(ResultSet),
  Void,
  Ready,
}

#[allow(dead_code)]
#[derive(Debug, Clone, Copy)]
#[repr(i32)]
pub enum ResultKind {
  Void = 0x01,
  Rows = 0x02,
  SetKeyspace = 0x03,
  Prepared = 0x04,
  SchemaChange = 0x05,
}

#[allow(dead_code)]
#[derive(Debug, Clone, Copy)]
#[repr(i32)]
pub enum RowFlags {
  GlobalTablesSpec = 0x01,
  HasMorePages = 0x02,
  NoMetadata = 0x04,
}

#[allow(dead_code)]
#[derive(Debug, Clone, Copy)]
#[repr(u8)]
pub enum QueryFlags {
  Values = 0x01,
  SkipMetadata = 0x02,
  PageSize = 0x04,
  WithPagingState = 0x08,
  WithSerialConsistency = 0x10,
  WithDefaultTimestamp = 0x20,
  WithNamesForValues = 0x40,
}

#[derive(Debug)]
pub struct Row {
  pub columns: Vec<Option<Vec<u8>>>,
}

#[derive(Debug)]
pub struct Column {
  pub name: String,
  pub type_id: ColumnTypeId,
}

#[derive(Debug)]
pub struct ResultSet {
  pub metadata: Option<Vec<Column>>,
  pub rows: Vec<Row>,
  paging_state: Option<Vec<u8>>,
}

impl Row {
  pub fn with_columns(count: usize) -> Self {
    Row {
      columns: Vec::with_capacity(count),
    }
  }
}

#[derive(Debug, Clone)]
pub enum Value {
  Text(String),
  Boolean(bool),
}

#[derive(Debug, Clone)]
pub struct Query {
  query: String,
  positional_parameters: Option<Vec<Value>>,
  named_parameters: Option<HashMap<String, Value>>,
}

impl Query {
  pub fn new(query: String,
             positional_parameters: Option<Vec<Value>>,
             named_parameters: Option<HashMap<String, Value>>) -> Self {
    Query {
      query: query,
      positional_parameters: positional_parameters,
      named_parameters: named_parameters,
    }
  }
}

named!(pub parse_header<&[u8], Header>,
  do_parse!(
    version: be_u8 >>
    flags: be_u8   >>
    stream: be_i16 >>
    opcode: be_u8  >>
    length: be_i32 >>
    ( Header {
      version: version,
      flags: flags,
      stream: stream,
      opcode: unsafe { mem::transmute(opcode) },
      length: length,
    })
  )
);

fn parse_option<T: Read>(reader: &mut T) -> ColumnTypeId {
  let id = reader.read_u16::<BigEndian>().unwrap();
  let coltype: ColumnTypeId = unsafe { mem::transmute(id) };

  match coltype {
    ColumnTypeId::Ascii => coltype,
    ColumnTypeId::Boolean => coltype,
    ColumnTypeId::Varchar => coltype,
    ColumnTypeId::Int => coltype,
    _ => unimplemented!(),
  }
}

fn parse_string<T: Read>(reader: &mut T) -> String {
  let size = reader.read_u16::<BigEndian>().unwrap();
  let mut buf = Vec::with_capacity(size as usize);
  unsafe { buf.set_len(size as usize); }
  reader.read_exact(&mut buf).unwrap();
  String::from_utf8(buf).unwrap()
}

fn parse_error<T: Read>(reader: &mut T) -> CQLError {
  let code = reader.read_i32::<BigEndian>().unwrap();
  let s = parse_string(reader);

  CQLError {
    code: unsafe { mem::transmute(code) },
    message: s,
  }
}

fn parse_bytes<T: Read>(reader: &mut T) -> Option<Vec<u8>> {
  let n = reader.read_i32::<BigEndian>().unwrap();
  if n < 0 {
    None
  } else {
    let mut buf = Vec::with_capacity(n as usize);
    unsafe { buf.set_len(n as usize); }
    reader.read_exact(&mut buf).unwrap();
    Some(buf)
  }
}

struct SchemaChange {
  pub change_type: String,
  pub target: String,
  pub options: String,
}

fn parse_schema_changed<T: Read>(reader: &mut T) -> SchemaChange {
  let change_type = parse_string(reader);
  let target = parse_string(reader);
  let options;
  if target == "KEYSPACE" {
    options = parse_string(reader);
  } else {
    options = format!("{}.{}", parse_string(reader), parse_string(reader));
  }

  info!("SCHEMA_CHANGE: {} {} {}", change_type, target, options);

  SchemaChange {
    change_type, target, options
  }
}

fn parse_rows<T: Read>(reader: &mut T) -> ResultSet {
  let flags = reader.read_i32::<BigEndian>().unwrap();
  let global_tables_spec = flags & RowFlags::GlobalTablesSpec as i32 != 0;
  let has_more_pages = flags & RowFlags::HasMorePages as i32 != 0;
  let no_metadata = flags & RowFlags::NoMetadata as i32 != 0;
  let column_count = reader.read_i32::<BigEndian>().unwrap();
  let mut paging_state = None;
  let mut global_keyspace_name = None;
  let mut global_table_name = None;

  let mut col_meta = Vec::with_capacity(column_count as usize);

  if !no_metadata {
    if has_more_pages {
      paging_state = parse_bytes(reader);
    }
    if global_tables_spec {
      global_keyspace_name = Some(parse_string(reader));
      global_table_name = Some(parse_string(reader));
    }
    for _i in 0..column_count {
      let (_keyspace_name, _table_name) = if !global_tables_spec {
        (parse_string(reader), parse_string(reader))
      } else {
        (global_keyspace_name.clone().unwrap(), global_table_name.clone().unwrap())
      };
      let name = parse_string(reader);
      let coltype = parse_option(reader);
      col_meta.push(Column { name: name, type_id: coltype });
    }
  }

  let row_count = reader.read_i32::<BigEndian>().unwrap();

  let mut result = Vec::with_capacity(row_count as usize);

  for _i in 0..row_count {
    let mut row = Row::with_columns(column_count as usize);
    for _j in 0..column_count {
      row.columns.push(parse_bytes(reader));
    }
    result.push(row);
  }
  ResultSet {
    metadata: if !no_metadata { Some(col_meta) } else { None },
    rows: result,
    paging_state: paging_state,
  }
}

fn write_value_bytes<T: Write>(writer: &mut T, v: &Value) -> Result<usize, io::Error> {
  match v {
    &Value::Text(ref t) => {
      writer.write_i32::<BigEndian>(t.len() as i32)?;
      writer.write(t.as_bytes())
    },
    &Value::Boolean(b) => {
      writer.write_i32::<BigEndian>(1)?;
      writer.write_u8(if b { 0x01 } else { 0x00 } )?;
      Ok(1)
    }
  }
}

fn write_string<T: Write>(writer: &mut T, s: &str) -> Result<usize, io::Error> {
  writer.write_u16::<BigEndian>(s.len() as u16)?;
  writer.write(s.as_bytes())
}

fn write_long_string<T: Write>(writer: &mut T, s: &str) -> Result<usize, io::Error> {
  writer.write_i32::<BigEndian>(s.len() as i32)?;
  writer.write(s.as_bytes())
}

fn write_map<T: Write>(writer: &mut T, m: &HashMap<String, String>) -> Result<usize, io::Error> {
  writer.write_u16::<BigEndian>(m.len() as u16)?;
  for (k, v) in m {
    write_string(writer, k)?;
    write_string(writer, v)?;
  }
  Ok(0)
}

#[allow(dead_code)]
pub fn start() -> Message {
  let mut options = HashMap::new();
  options.insert("CQL_VERSION".to_string(), "3.0.0".to_string());
  Message::Startup(options)
}

pub fn query(query: Query) -> Message {
  if query.positional_parameters.is_none() && query.named_parameters.is_none() {
    Message::Query(query.query, Consistency::One)
  } else if query.positional_parameters.is_some() {
    Message::QueryWithParams(query.query, query.positional_parameters.unwrap(), Consistency::One)
  } else {
    Message::QueryWithNamedParams(query.query, query.named_parameters.unwrap(), Consistency::One)
  }
}

pub fn write_message<T: Write>(stream: i16, msg: &Message, writer: &mut T) -> Result<usize, io::Error> {
  writer.write_u8(0x03)?;
  writer.write_u8(0x00)?;
  writer.write_i16::<BigEndian>(stream)?;

  let buf: Vec<u8> = vec![];
  let cs = &mut Cursor::new(buf);
  match msg {
    &Message::Startup(ref options) => {
      writer.write_u8(Opcode::Startup as u8)?;
      write_map(cs, options)?;
    },
    &Message::Query(ref s, ref consistency) => {
      writer.write_u8(Opcode::Query as u8)?;
      write_long_string(cs, s)?;
      cs.write_u16::<BigEndian>(*consistency as u16)?;
      cs.write_u8(0x00)?;
    },
    &Message::QueryWithParams(ref s, ref params, ref consistency) => {
      writer.write_u8(Opcode::Query as u8)?;
      write_long_string(cs, s)?;
      cs.write_u16::<BigEndian>(*consistency as u16)?;
      cs.write_u8(QueryFlags::Values as u8)?;
      cs.write_u16::<BigEndian>(params.len() as u16)?;
      for v in params {
        write_value_bytes(cs, v)?;
      }
    },
    &Message::QueryWithNamedParams(ref s, ref params, ref consistency) => {
      writer.write_u8(Opcode::Query as u8)?;
      write_long_string(cs, s)?;
      cs.write_u16::<BigEndian>(*consistency as u16)?;
      cs.write_u8(QueryFlags::Values as u8 | QueryFlags::WithNamesForValues as u8)?;
      cs.write_u16::<BigEndian>(params.len() as u16)?;
      for (n, v) in params {
        write_string(cs, n)?;
        write_value_bytes(cs, v)?;
      }
    },
    _ => unimplemented!(),
  }
  writer.write_i32::<BigEndian>(cs.get_ref().len() as i32)?;
  writer.write_all(cs.get_ref())?;
  Ok(0)
}

fn parse_message(opcode: Opcode, buf: &[u8]) -> Message {
  match opcode {
    Opcode::Error => {
      let err = parse_error(&mut Cursor::new(buf));
      return Message::Error(err);
    },
    Opcode::Ready => {
      return Message::Ready;
    },
    Opcode::Result => {
      let mut reader = Cursor::new(buf);
      let rs = reader.read_i32::<BigEndian>().unwrap();
      let kind: ResultKind = unsafe { mem::transmute(rs) };
      return match kind {
        ResultKind::Rows => {
          Message::Result(parse_rows(&mut reader))
        },
        ResultKind::Void => {
          Message::Void
        },
        ResultKind::SchemaChange => {
          parse_schema_changed(&mut reader);
          unimplemented!()
        },
        _ => {
          info!("Unsupported ResultKind {:?} {}", kind, kind as u8);
          unimplemented!()
        }
      }
    },
    opc => {
      println!("got opc: {:?}", opc);
      unimplemented!()
    }
  }
}

pub enum ParseError {
  NotEnoughData,
  ParseError
}

pub fn read_message(buf: &[u8]) -> Result<(i32, (i16, Message)), ParseError> {
  match parse_header(&buf)  {
    IResult::Ok((_, header)) => {
      
      if buf.len() as i32 >= header.length + 9 {
        Ok((header.length+9, (header.stream, parse_message(header.opcode, &buf[9..]))))
      } else {
        Err(ParseError::NotEnoughData)
      }
    },
    IResult::Err(nom::Err::Incomplete(_)) => Err(ParseError::NotEnoughData),
    IResult::Err(_) => Err(ParseError::ParseError)
  }
}

impl Message {
  pub fn as_bytes(&self) -> Vec<u8> {
    let res = Vec::new();
    let mut cursor = Cursor::new(res);
    write_message(0, self, &mut cursor).unwrap();
    cursor.into_inner()
  }
}
