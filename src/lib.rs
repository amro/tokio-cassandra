#![feature(async_closure)]
#![warn(rust_2018_idioms)]

#![allow(dead_code)]

mod api;
mod client;
mod codec;
mod cql;
mod session;

pub use crate::client::Client;

pub use crate::cql::Message;
pub use crate::cql::query;
pub use crate::api::QueryBuilder;
pub use crate::api::QueryResult;
