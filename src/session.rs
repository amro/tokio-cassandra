use std::error::Error;

use crate::Client;
use crate::cql::Query;
use crate::cql::ResultSet;

pub struct Session {
  clients: Vec<Client>
}

impl Session {
  pub async fn init(_address: &str) -> Result<Self, Box<dyn Error>> {
    Ok(Session {
      clients: Vec::new(),
    })
  }

  pub async fn query(&self, _query: Query) -> Result<ResultSet, Box<dyn Error>> {
    // Box::new(self.client.call(protocol::query(query)).map(|message| {
    //   match message {
    //     Message::Result(rs) => {
    //       rs
    //     },
    //     _ => {
    //       unimplemented!()
    //     }
    //   }
    // }))
    unimplemented!()
  }

  pub async fn insert(&self, _query: Query) -> Result<(), Box<dyn Error>> {
    // Box::new(self.client.call(protocol::query(query)).map(|message| {
    //   match message {
    //     Message::Void => {},
    //     Message::Error(e) => {
    //       println!("error: {:?}", e);
    //       unimplemented!()
    //     }
    //     _ => {
    //       unimplemented!()
    //     }
    //   };
    // }))
    unimplemented!()
  }
}
